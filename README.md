#overview
This is a dockerized environment that spins up two docker instances.

One instance runs the app, the other instance is the database.

The entire package is self-contained and grabs changes as they are saved.

Utilizing [pytest](https://pytest-django.readthedocs.io/en/latest/#why-would-i-use-this-instead-of-django-s-manage-py-test-command) to run tests. 

Using environment variables for dev and prod. 
- prod files live in .env
- dev settings live in .env.dev 
Mounts an entrypoint.sh file to wait for the local database to spin up.

#Starting the environment
docker-compose up -d --build

#Teardown environment
docker-compose down -v

#Monitoring the logs 
docker-compose logs -f 

#Running Tests
docker-compose exec movies pytest

#Executing Commands
docker-compose exec movies python manage.py migrate

#Tests with coverage 
 docker-compose exec movies pytest -p no:warnings --cov=.